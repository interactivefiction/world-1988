# World 1988
As stated in the original Readme:

«This is the portable "C" version of my text adventure game "WORLD".
It is a large game in the flavor of Adventure or Zork, and about as large.
It is, however, a "sci-fi" type game somewhat like the commercial
Infocom games "Planetfall" or "Starcross", but much larger.»

## Description
Compile World on Linux

## Installation
Not required

## Usage
### Building
Run `make` in the code's directory

### Playing
Navigate to `build` directory from the terminal

Run `./world` make sure `q1text.dat` is in the same directory

## Contributing
Contributions are more than welcome :)

## Authors and acknowledgment
All game code is copyright 1992 J.D.McDonald

## License
World C is free for non-commercial purposes.
